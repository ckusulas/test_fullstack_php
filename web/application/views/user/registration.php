<div class="container">

    <!-- Include Flash Data File -->
         <?php $this->load->view('FlashAlert/flash_alert.php') ?>
    <?= form_open() ?>
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="first_name" value="<?= set_value('first_name'); ?>" class="form-control <?= (form_error('first_name') == "" ? '':'is-invalid') ?>" placeholder="Ingrese Nombre">
            <?= form_error('first_name'); ?>        
        </div>
        <div class="form-group">
            <label>Apellido</label>
            <input type="text" name="second_name" value="<?= set_value('second_name'); ?>" class="form-control <?= (form_error('second_name') == "" ? '':'is-invalid') ?>" placeholder="Ingrese Apellido">  
            <?= form_error('second_name'); ?>           
        </div>
        <div class="form-group">
            <label>Correo</label>
            <input type="email" name="email" value="<?= set_value('email'); ?>" class="form-control <?= (form_error('email') == "" ? '':'is-invalid') ?>" placeholder="Ingrese Correo"> 
            <?= form_error('email'); ?>            
        </div>  
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" value="<?= set_value('password'); ?>" class="form-control <?= (form_error('password') == "" ? '':'is-invalid') ?>" placeholder="Password">
            <?= form_error('password'); ?> 
        </div>
        <div class="form-group">
            <label>Confirmacion de Password</label>
            <input type="password" name="passconf" value="<?= set_value('passconf'); ?>" class="form-control <?= (form_error('passconf') == "" ? '':'is-invalid') ?>" placeholder="Password">
            <?= form_error('passconf'); ?> 
        </div>
        <button type="submit" class="btn btn-primary">Register</button>
    <?= form_close() ?>
</div>
<br>