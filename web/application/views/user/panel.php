<div class="container">

    <!-- Include Flash Data File -->
    <?php $this->load->view('FlashAlert/flash_alert.php') ?>
    
    <div class="jumbotron">
    	 
        <h1 class="display-4">Bienvenido <?= $this->session->userdata('USER_NAME') ?></h1>
        
        <hr class="my-4">
        <a class="btn btn-primary btn-lg" href="../examples/construction_management/" role="button">Crud Construcciones</a>
      
    </div>
</div>