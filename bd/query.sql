CREATE DATABASE Testing_fullstack;
USE Testing_fullstack; 

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `buildings`
-- ----------------------------
DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings` (
  `building_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Clave` varchar(20) DEFAULT NULL,
  `Delegacion` varchar(255) DEFAULT NULL,
  `Colonia` varchar(255) DEFAULT NULL,
  `Calle` varchar(255) DEFAULT NULL,
  `Posicionamiento` varchar(1000) DEFAULT '3',
  `Caracteristicas` text,
  PRIMARY KEY (`building_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of buildings
-- ----------------------------
INSERT INTO `buildings` VALUES ('1', 'PCOM-ABC/01', 'Benito Juarez', 'Narvarte', 'Peten', '19.3789349,-99.1549417,20.75z', '<p>\r\n	Restaurante exquisito</p>\r\n');
INSERT INTO `buildings` VALUES ('2', 'PCOM-FAC/02', 'Iztacalco', 'Viaducto Piedad', 'Albino Garcia', '19.4018748,-99.1349363', '<p>\r\n	Restaurante pozoleria</p>\r\n');
INSERT INTO `buildings` VALUES ('3', 'PCOM-TBG/03', 'Coyoacan', 'Churubusco Campestre', 'Cerro Boludo 11', '19.3440271,-99.1329219', '<p>\r\n	TIntoreria al lado</p>\r\n');
INSERT INTO `buildings` VALUES ('4', 'PCOM-RFS/04', 'Benito Juarez', 'Napoles', 'Montecito', '19.393664,-99.1767918', '<p>\r\n	WTC Hotel de Mexico</p>\r\n');
INSERT INTO `buildings` VALUES ('6', 'PCOM-IFR/05', 'Cuauhtemoc', 'Reforma', 'Paseo de la Reforma', '19.4242447,-99.1774158', '<p>\r\n	Torrre Mayor</p>\r\n');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Is_active` tinyint(4) DEFAULT NULL,
  `Created_at` datetime DEFAULT NULL,
  `Update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Nathan', 'Smith', 'pcom@gmail.com', '3dd14afc9f2da6c03c4f6599553a4597', null, null, null);
INSERT INTO `users` VALUES ('5', 'constantino', 'kusulas', 'constantinokv@gmail.com', '$2y$10$I0gTY9Qlb0TGjbWpo6csL.G0/4pE7qlrg0FQahSWTW0/pVSiC2NYW', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');