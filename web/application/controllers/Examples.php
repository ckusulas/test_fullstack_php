<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examples extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('example.php',(array)$output);
	}

 



	public function construction_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('buildings');
		//$crud->set_relation_n_n('actors', 'film_actor', 'actor', 'film_id', 'actor_id', 'fullname','priority');
		//$crud->set_relation_n_n('category', 'film_category', 'category', 'film_id', 'category_id', 'name');
		//$crud->unset_columns('special_features','description','actors');

		$crud->fields('Clave', 'Delegacion', 'Colonia' ,  'Calle' ,'Posicionamiento', 'Caracteristicas');

		$output = $crud->render();

		$this->_example_output($output);
	}


	 

}
